ARG ALPINE_VERSION=3.20
FROM alpine:$ALPINE_VERSION AS pydeps

RUN apk add --no-cache \
	python3 \
	py3-chardet \
	py3-cryptography \
	py3-feedparser \
	py3-gunicorn \
	py3-jinja2 \
	py3-markdown \
	py3-netaddr \
	py3-packaging \
	py3-pillow \
	py3-psycopg-c \
	py3-psycopg-pool \
	py3-requests  \
	py3-requests \
	py3-tzdata \
	py3-yaml

FROM pydeps AS builder

ARG NETBOX_VERSION=v4.1.8
ARG NETBOX_BRANCHING_VERSION=0.5.1

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

COPY patches /tmp
COPY --chmod=0755 build /usr/local/bin

RUN apk --no-cache add \
	build-base \
	git \
	python3-dev \
	py3-pip \
	py3-wheel \
	patch

RUN git -c advice.detachedHead=false clone --branch "$NETBOX_VERSION" --depth=1 \
	https://github.com/netbox-community/netbox.git /root/netbox \
	&& rm -rf /root/netbox/.git \
	&& patch -d /root/netbox -p1 -i /tmp/netbox-defaults.patch

RUN MAKEFLAGS="-j $(nproc)" && export MAKEFLAGS && \
	fix-deps.sh /root/netbox/requirements.txt && \
	printf "netboxlabs-netbox-branching==%s\n" "$NETBOX_BRANCHING_VERSION" >>/root/netbox/requirements.txt && \
	pip3 install \
	--no-cache-dir \
	--no-warn-script-location \
	--prefix=/root/install -r /root/netbox/requirements.txt

FROM pydeps

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

WORKDIR /srv/netbox/netbox

COPY --from=builder /root/install /usr
COPY --from=builder /root/netbox /srv/netbox

COPY scripts /usr/local/bin
COPY config/local_settings.py /srv/netbox/netbox/netbox/local_settings.py

RUN <<EOF
	apk --no-cache add postgresql-client curl
	ln -sf /srv/netbox/netbox/manage.py /usr/local/bin/netbox
	addgroup -S netbox
	adduser -S -H -h /srv/netbox -s /sbin/nologin -G netbox -g netbox netbox
	mkdir -p /srv/netbox/netbox/static
	cp /srv/netbox/netbox/netbox/configuration_example.py \
		/srv/netbox/netbox/netbox/configuration.py
	chown -R netbox:netbox /srv/netbox
	mv /var/spool/cron/crontabs/root /var/spool/cron/crontabs/netbox
	chown netbox: /var/spool/cron/crontabs/netbox
	ln -s /usr/local/bin/housekeeping.sh /etc/periodic/daily
EOF

USER netbox:netbox

ENTRYPOINT [ "entrypoint.sh" ]
