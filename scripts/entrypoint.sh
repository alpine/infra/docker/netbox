#!/bin/sh

set -eu

run() {
	netbox collectstatic --no-input

	# wait till postgres is online
	while ! pg_isready -qh postgres; do sleep 1; done
	netbox migrate
	netbox remove_stale_contenttypes --no-input
	netbox reindex --lazy

	if [ "$NETBOX_ADMIN_USER" ] && [ "$NETBOX_ADMIN_EMAIL" ] && [ "$NETBOX_ADMIN_PASS" ]; then
		netbox shell <<- EOF
		from users.models.users import User
		if not User.objects.filter(username='$NETBOX_ADMIN_USER'):
		    User.objects.create_superuser('$NETBOX_ADMIN_USER', '$NETBOX_ADMIN_EMAIL', '$NETBOX_ADMIN_PASS')
		EOF
	else
		echo "Skipping creating admin user. Make sure \$NETBOX_ADMIN_XXX variables are set"
	fi

	exec gunicorn --bind 0.0.0.0:8080 --workers 5 --threads 3 --timeout 120 \
		--max-requests 5000 --max-requests-jitter 500 netbox.wsgi

}

case ${1-} in
	sh)
		shift;
		if [ $# -gt 0 ]; then
			exec sh -c "$@"
		else
			exec sh -s
		fi
	;;
	run|"") run;;
	cron) exec crond -f -L /dev/stdout;;
	netbox) shift; exec netbox "$@";;
	*) exec netbox "$@";;
esac
