#!/bin/sh

set -eu

requirements_file=${1?Please provide path to requirements}

sed -i \
    -e '/feedparser/s/.*/feedparser>=6.0.11/' \
    -e '/gunicorn/s/.*/gunicorn>=21.2.0/' \
    -e '/Jinja2/s/.*/Jinja2>=3.1.4/' \
    -e '/Markdown/s/.*/Markdown>=3.6/' \
    -e '/netaddr/s/.*/netaddr>=0.10.0/' \
    -e '/Pillow/s/.*/Pillow>=10.3.0/' \
    -e '/psycopg/s/.*/psycopg[c,pool]>=3.1.19/' \
    -e '/PyYAML/s/.*/PyYAML>=6.0.1/' \
    -e '/requests/s/.*/requests>=2.31.0/' \
    -e '/svgwrite/s/.*/svgwrite>=1.4.3/' \
    -e '/tzdata/s/.*/tzdata>=2024.1/' \
    "$requirements_file"
